
import _ from 'lodash'
import moment from 'moment'
import styled from 'styled-components'

const TransactionListItem = (item: { 
  id: string,
  category_title: string,
  description: string,
  date: string,
  amount: {
    value: number
    currency_iso: string
  }
}) => {

  return (
    <>
      <Item>
        <div>
          <span>
            {item.category_title}
          </span>
          <Title>
            {item.description}
          </Title>
          <div className="date">
            <span>{moment(`${item.date}`).format('dddd, MMMM Do YYYY')}</span>
          </div>
        </div>
        <div>
          <Price>
            {item.amount.currency_iso === 'GBP' && 
              <span>£</span>
            }
            <span>{item.amount.value}</span>
          </Price>
        </div>
      </Item>
    </>
  )
}

const Item = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: #fff;
  padding: 15px;
  border-radius: 10px;
  margin-bottom: 20px;
  box-shadow: 1px 3px 26px rgb(0 0 0 / 9%);
`;
  
const Title = styled.h2`
  font-size: 24px;
  margin: 0;
`;
  
const Price = styled.p`
  font-weight: bold
`;

export default TransactionListItem