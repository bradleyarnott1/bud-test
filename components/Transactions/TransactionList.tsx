import TransactionListItem from "./TransactionListItem"
import _ from 'lodash'
import styled from 'styled-components'

const TransactionList = (
  item: {
    transactions: Array<{
      id: string,
      category_title: string,
      description: string,
      date: string,
      amount: {
        value: number
        currency_iso: string
      }
    }>,
  }
  ) => {


  let items = null;
  const data = item.transactions;
    
  if (data) {

    const filterItems = data.sort((a,b) => (b.amount.value - a.amount.value)).filter(o => o.amount.value < 0).slice(0, 10);

    const newArray = _.orderBy(filterItems, 'date', 'desc');

    items = newArray.map((item, i) => {
      return <TransactionListItem key={i} {...item} />;
    });
  }

  return (
    <>
      <List>
        {data ? items : <span>no results</span>}
      </List>
    </>
  )
}

const List = styled.div`
  display: flex;
  flex-flow: column;
  padding: 20px;
`;

export default TransactionList