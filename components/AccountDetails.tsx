import Image from 'next/image'
import styled from 'styled-components'

const AccountDetails = (provider: any, balance: any) => {

  return (
    <>
      <Wrapper>
        <Card>
          { provider.provider.title === 'Monzo'
            ?
            <Image
              src="/monzo-bank-card.png"
              alt="Monzo Bank"
              layout="responsive"
              width={450}
              height={284}
            />
            :
            <p>g</p>
          }
        </Card>
        <Details>
          <Title>
            {provider.provider.description}
          </Title>
          <BankDetails>
            {provider.provider.sort_code}
          </BankDetails>
          <BankDetails>
            / {provider.provider.account_number}
          </BankDetails>
          <BalanceWrapper>
            <Balance>
              { provider.balance.currency_iso === 'GBP' && 
                <span>£</span>
              }
              {provider.balance.amount}
            </Balance>
          </BalanceWrapper>
        </Details>
      </Wrapper>
    </>
  )

}

const Wrapper = styled.div`
  position: sticky;
  top: 20px;
  display: flex;
  align-items: center;
  box-shadow: 1px 3px 26px rgb(0 0 0 / 9%);
  background: #fff;
  border-radius: 10px;
  padding: 20px;
  @media (max-width: 1200px) {
    flex-direction: column;
  }
`
const Card = styled.div`
  flex: 1;
  @media (max-width: 1200px) {
    width: 100%;
  }
`

const Details = styled.div`
  padding-left: 20px;
  flex: 1;
  @media (max-width: 1200px) {
    padding: 20px 0 0;
  }
`

const Title = styled.h2`
  font-size: 32px;
  margin: 0 0 10px;
`

const BankDetails = styled.span`
  font-size: 16px;
  color: #616161;
  margin: 0 5px 0 0;
`

const BalanceWrapper = styled.div`
  margin-top: 10px;
`

const Balance = styled.h3`
  margin-top: 0
`

export default AccountDetails;