import Head from 'next/head';
import { GetStaticProps } from 'next'
import styled from 'styled-components'

import { TransactionList, AccountDetails } from '../components/index';

const Home = ({test}) => {

  return (
    <>
      <Head>
        <title>Bud Test</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div>
        <Columns>
          <ColmunSmall>
            <AccountDetails provider={test.provider} balance={test.balance} />
          </ColmunSmall>
          <ColmunLarge>
            <TransactionList transactions={test.transactions}/>
          </ColmunLarge>
        </Columns>
      </div>
    </>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  const res = await fetch('http://www.mocky.io/v2/5c62e7c33000004a00019b05')
  const test = await res.json()

  if (!test) {
    return {
      notFound: true,
    }
  }
  
  return {
    props: {
      test
    },
  }
}

const Columns = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

const ColmunSmall = styled.div`
  width: 600px;
  padding-left: 20px;
  @media (max-width: 1200px) {
    width: 300px;
  }
  @media (max-width: 768px) {
    width: 100%;
    padding: 0 20px;
  }
`

const ColmunLarge = styled.div`
  flex: 4;
`

export default Home;
