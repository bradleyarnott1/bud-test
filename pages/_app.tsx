import { createGlobalStyle, ThemeProvider } from 'styled-components'
import '../styles/globals.css'

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    box-sizing: border-box;
    background: #fcfcfc
  }
`
function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
